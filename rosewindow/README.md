# RoseWindow README

RoseWindow is an extension to enable working with the Rosie Pattern Language.  This is the very first version of the extension.  If you care to log issues or make suggestions, please go to https://www.gitlab.com/OnorioCatenacci/RoseWindow.